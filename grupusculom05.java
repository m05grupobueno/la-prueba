package com.company;

import java.util.Scanner;
public class Main{


    public static void main (String[] args){
        Scanner input= new Scanner(System.in);
        System.out.println("Wenas, bienvenido a que numero hay escrito en el codigo que todos pueden ver");
        int unmisterio = (int)(Math.random()*(10-1)+1);
        int count = 3;
        boolean win = false;
        while (count>0 && !win){
            System.out.println("Introduce el numero");
            int num= input.nextInt();
            input.nextLine();
            count--;
            if (num==unmisterio){
                win=true;
            }
        }
        if (win){ System.out.println("Has ganado"); }
        else {
            System.out.println("Pierdes! Viva la inteligencia emocional. Lo que buscabas era.."+unmisterio);
        }
    }
}