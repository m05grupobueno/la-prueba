package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int tiposBillete = 5;
        char zona;
        String[][] compra = new String[3][2];
        float[] pt =  new float[3];
        float pap;


        float billeteSencillo = 2.40f;
        float tcasual = 11.35f;
        float tusual = 40.00f;
        float tfamiliar = 10.00f;
        float tjoven = 80.00F;

        float zona1 = 1.0F;
        float zona2 = 1.3125F;
        float zona3 = 1.8443F;


        do {
            for (int i = 0; i < 3; i++) {


                //#####################MENU######################

                System.out.println("##################MENU##################");
                System.out.println("----------------------------------------");
                System.out.println("|  Bienvenido a la maquina de billetes |");
                System.out.println("|  Seleccione un tipo de Billete       |");
                System.out.println("----------------------------------------");

                System.out.println(1 + " - Billete Sencillo");
                System.out.println(2 + " - Tcasual");
                System.out.println(3 + " - TUsual");
                System.out.println(4 + " - TFamiliar");
                System.out.println(5 + " - TJoven");

                tiposBillete = input.nextInt();

                //#####################ZONA######################

                System.out.println("##################ZONAS#################");
                System.out.println("----------------------------------------");
                System.out.println("             Escoja una Zona");
                System.out.println("----------------------------------------");
                System.out.println('a' + " - Zona 1");
                System.out.println('b' + " - Zona 2");
                System.out.println('c' + " - Zona 3");
                System.out.println("----------------------------------------");

                zona = input.next().charAt(0);


                switch (tiposBillete) {
                    case 1:
                        System.out.println("Has seleccionado el Billete Sencillo");
                        compra[i][0] = "Billete Sencillo";
                        //System.out.println("Selecione una Zona");
                        switch (zona) {
                            case 'a':
                                System.out.println('a' + " Zona 1, con un precio de " + billeteSencillo + "€");
                                compra[i][1] = "Zona 1";
                                pt[i] = billeteSencillo;
                                break;
                            case 'b':
                                System.out.println('b' + " Zona 2, con un precio de " + (billeteSencillo * zona2) + "€");
                                compra[i][1] = "Zona 2";
                                pt[i] = (billeteSencillo * zona2);
                                break;
                            case 'c':
                                System.out.println('c' + " Zona 3, con un precio de " + (billeteSencillo * zona3) + "€");
                                compra[i][1] = "Zona 3";
                                pt[i] = (billeteSencillo * zona3);
                                break;
                        }
                        break;
                    case 2:
                        System.out.println("Has seleccionado el Billete TCasual");
                        compra[i][0] = "TCasual";
                        //System.out.println("Selecione una Zona");
                        switch (zona) {
                            case 'a':
                                System.out.println('a' + " Zona 1, con un precio de " + tcasual + "€");
                                compra[i][1] = "Zona 1";
                                pt[i] = tcasual;
                                break;
                            case 'b':
                                System.out.println('b' + " Zona 2, con un precio de " + (tcasual * zona2) + "€");
                                compra[i][1] = "Zona 2";
                                pt[i] = (tcasual * zona2);
                                break;
                            case 'c':
                                System.out.println('c' + " Zona 3, con un precio de " + (tcasual * zona3) + "€");
                                compra[i][1] = "Zona 3";
                                pt[i] = (tcasual * zona3);
                                break;
                        }
                        break;
                    case 3:
                        System.out.println("Has seleccionado el Billete TUsual");
                        compra[i][0] = "TUsual";
                        //System.out.println("Selecione una Zona");
                        switch (zona) {
                            case 'a':
                                System.out.println('a' + " Zona 1, con un precio de " + tusual + "€");
                                compra[i][1] = "Zona 1";
                                pt[i] = tusual;
                                break;
                            case 'b':
                                System.out.println('b' + " Zona 2, con un precio de " + (tusual * zona2) + "€");
                                compra[i][1] = "Zona 2";
                                pt[i] = (tusual * zona2);
                                break;
                            case 'c':
                                System.out.println('c' + " Zona 3, con un precio de " + (tusual * zona3) + "€");
                                compra[i][1] = "Zona 3";
                                pt[i] = (tusual * zona3);
                                break;
                        }
                        break;
                    case 4:
                        System.out.println("Has seleccionado el Billete TFamiliar");
                        compra[i][0] = "TFamiliar";
                        //System.out.println("Selecione una Zona");
                        switch (zona) {
                            case 'a':
                                System.out.println('a' + " Zona 1, con un precio de " + tfamiliar + "€");
                                compra[i][1] = "Zona 1";
                                pt[i] = tfamiliar;
                                break;
                            case 'b':
                                System.out.println('b' + " Zona 2, con un precio de " + (tfamiliar * zona2) + "€");
                                compra[i][1] = "Zona 2";
                                pt[i] = (tfamiliar * zona2);
                                break;
                            case 'c':
                                System.out.println('c' + " Zona 3, con un precio de " + (tfamiliar * zona3) + "€");
                                compra[i][1] = "Zona 3";
                                pt[i] = (tfamiliar * zona3);
                                break;
                        }
                        break;
                    case 5:
                        System.out.println("Has seleccionado el Billete TJoven");
                        compra[i][0] = "TJoven";
                        //System.out.println("Selecione una Zona");
                        switch (zona) {
                            case 'a':
                                System.out.println('a' + " Zona 1, con un precio de " + tjoven + "€");
                                compra[i][1] = "Zona 1";
                                pt[i] = tjoven;
                                break;
                            case 'b':
                                System.out.println('b' + " Zona 2, con un precio de " + (tjoven * zona2) + "€");
                                compra[i][1] = "Zona 2";
                                pt[i] = (tjoven * zona2);
                                break;
                            case 'c':
                                System.out.println('c' + " Zona 3, con un precio de " + (tjoven * zona3) + "€");
                                compra[i][1] = "Zona 3";
                                pt[i] = (tjoven * zona3);
                                break;
                        }
                        break;
                    default:
                        System.out.println("El billete seleccionado no existe o no se encuentra disponible");
                        break;
                }

                if (i != 2) {
                    System.out.println("¿Quieres comprar otro billete?");
                    char deci = input.next().charAt(0);
                    if (deci == 's') {
                    }
                    else{
                        i = 3;
                    }
                }
            }
            float preciot = pt[0] + pt[1] + pt[2];

            //#####################A PAGAR######################

            System.out.println("#################A PAGAR################");
            System.out.println("----------------------------------------");
            System.out.println("El precio total a pagar es de: " +preciot+ "€");
            System.out.println("----------------------------------------");
            System.out.println("Introduzca en EUROS la cantidad a pagar");
            pap = input.nextFloat();
            System.out.println("El cambio es de: "  +(pap - preciot)+ "€");
            System.out.println("----------------------------------------");

            //#####################TIQUET######################

            System.out.println("#################TICKET#################");
            System.out.println("----------------------------------------");
            for (int i = 0; i < 3; i++) {
                System.out.println(compra[i][0]+ "(" +compra[i][1]+ ") --> " +pt[i]+"€");
            }
            System.out.println("----------------------------------------");

            System.out.println("----------------------------------------");
            System.out.println("Gracias por su compra");
            System.out.println("Que tenga un buen dia");
            System.out.println("----------------------------------------");


        } while (true == true);
        System.out.println("YO SOY GROOOOOT");
        
    }}